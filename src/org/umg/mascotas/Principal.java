package org.umg.mascotas;

/**
 * Created by alumno on 5/07/2017.
 */
public class Principal {

    public static void main(String[] args) {

        Tienda tienda = new Tienda();
        tienda.setNombreGerente("Juan Carlos Figueroa");
        tienda.setDireccion("Guatemala zona 3");
        tienda.setAnioCreacion(2013);

        Mascota mascota1 = new Mascota("Tipo A","Chiguagua",6);
        Mascota mascota2 = new Mascota("Tipo A","Dalmata",2);
        Mascota mascota3 = new Mascota("Tipo C","Pequines",4);
        Mascota mascota4 = new Mascota("Tipo B","Gran Danes",1);
        Mascota mascota5 = new Mascota("Tipo B","Pitbul",8);
        Mascota mascota6 = new Mascota("Tipo C","Buld Dog",4);

        tienda.addMasquota(mascota1);
        tienda.addMasquota(mascota2);
        tienda.addMasquota(mascota3);
        tienda.addMasquota(mascota4);
        tienda.addMasquota(mascota5);
        tienda.addMasquota(mascota6);

        System.out.println("El tota de años es: "+tienda.getTotal());


    }
}
