package org.umg.mascotas;

/**
 * Created by alumno on 5/07/2017.
 */
public class Mascota {

    private String tipoAnimal;
    private String raza;
    private int edad;

    public Mascota(String tipoAnimal, String raza, int edad) {
        this.tipoAnimal = tipoAnimal;
        this.raza = raza;
        this.edad = edad;
    }

    public String getTipoAnimal() {
        return tipoAnimal;
    }

    public void setTipoAnimal(String tipoAnimal) {
        this.tipoAnimal = tipoAnimal;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }




}
