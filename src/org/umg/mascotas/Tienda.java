package org.umg.mascotas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alumno on 5/07/2017.
 */
public class Tienda {
    private String nombreGerente;
    private String direccion;
    private int anioCreacion;
    private List<Mascota> mascotas;

    public Tienda() {
        mascotas=new ArrayList<>();
    }

    public Tienda(String nombreGerente, String direccion, int anioCreacion, List<Mascota> mascotas) {
        this.nombreGerente = nombreGerente;
        this.direccion = direccion;
        this.anioCreacion = anioCreacion;
        this.mascotas = mascotas;
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getAnioCreacion() {
        return anioCreacion;
    }

    public void setAnioCreacion(int anioCreacion) {
        this.anioCreacion = anioCreacion;
    }

    public List<Mascota> getMascotas() {
        return mascotas;
    }

    public void setMascotas(List<Mascota> mascotas) {
        this.mascotas = mascotas;
    }

    public void addMasquota(Mascota m){
        this.mascotas.add(m);
    }

    public int getTotal(){
        int suma = 0;
        for(Mascota e : this.mascotas)
            suma += e.getEdad();
        return suma;
    }
}
